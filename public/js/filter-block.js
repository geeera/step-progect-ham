// Filter BLOCK
const amazingBlockNav = document.querySelectorAll('.amazing-filter-block .amazing-list-item');
const photoBlock = document.querySelectorAll('.part-photo-block .photo-block');
let array = photoBlock;
let count = 12;
const button = document.getElementById('load-amazing-photo');

photoBlock.forEach(() => {
    if(array.length > count) {
        for (let i = 12; i < array.length; i++) {
            array[i].style.display = 'none';
        }
    }
});

amazingBlockNav.forEach((item) => {
    item.addEventListener('click', function () {
        const searchActive = this.closest('.universal-list-block').querySelector('.amazing-list-item-active');
        searchActive.classList.remove('amazing-list-item-active');
        this.classList.add('amazing-list-item-active');

        array = [];
        photoBlock.forEach((elem) => {
            elem.style.display = 'none';
            if(elem.classList.contains(this.dataset.sort)) {
                array.push(elem);
                elem.style.display = 'block';
            }
        });
        count = 12;
        array.forEach(() => {
            if(array.length > count) {
                for (let i = 12; i < array.length; i++) {
                    array[i].style.display = 'none';
                }
            }
        });
        button.style.display = 'block';
    });
});

button.addEventListener('click', function () {
    setTimeout(load, 1);
    setTimeout(loadHidden, 3000);
    setTimeout(displayPhoto, 3000);
    count += 12;
    console.log(array.length);
    if(count >= array.length) {
        this.style.display = 'none';
        count++;
    }

    function displayPhoto() {
        photoBlock.forEach(() => {
            if(array.length > count) {
                for (let i = 0; i < count; i++) {
                    array[i].style.display = 'block';
                }
            } else {
                for (let key = 0; key < array.length; key++) {
                    array[key].style.display = 'block';
                }
            }
        });
    }

    function load() {
        const loader = document.querySelector('#loader-block');
        loader.style.display = 'flex';
    }
    function loadHidden() {
        const loader = document.querySelector('#loader-block');
        loader.style.display = 'none';
    }
});