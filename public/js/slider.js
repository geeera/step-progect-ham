// Slider
const carousel = document.querySelectorAll('.user-photo-small-block');
const mainContent = document.querySelectorAll('.slide');
const next = document.getElementById('right-arrow');
const previous = document.getElementById('left-arrow');

let currentSlide = 0;

function goToSlide(nextSlide) {
    mainContent[currentSlide].closest('.slider-nav').querySelector('.active-slide').classList.remove('active-slide');
    carousel[currentSlide].closest('.users-block').querySelector('.active').classList.remove('active');
    console.log(carousel[currentSlide].classList);
    mainContent[nextSlide].classList.add('active-slide');
    carousel[nextSlide].classList.add('active');

    currentSlide = nextSlide;
    console.log(currentSlide);

    console.log(mainContent.length);
}

previous.onclick = function(e) {
    e.preventDefault();
    const nextSlide = currentSlide === 0 ? mainContent.length - 1 : currentSlide - 1;
    goToSlide(nextSlide);
};

next.onclick = function(e) {
    e.preventDefault();
    const nextSlide = currentSlide === mainContent.length - 1 ? 0 : currentSlide + 1;
    goToSlide(nextSlide);
};

carousel.forEach((listItem, index) => {listItem.addEventListener('click', function(event) {
    event.preventDefault();
    goToSlide(index);
})});